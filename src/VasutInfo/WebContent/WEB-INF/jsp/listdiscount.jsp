<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="List of Discounts">
	<s:layout-component name="body">
		<s:form beanclass="action.ListDiscountActionBean">
			<d:table name="${actionBean.discounts}" id="station" requestURI="" defaultsort="1"
					 defaultorder="ascending">
				<d:column title="Discount id" property="ID" sortable="true" />
				<d:column title="Discount name" property="name" sortable="true" />
				<d:column title="Discount percentage" property="percentage" sortable="true" />
			</d:table>
		</s:form>
	</s:layout-component>
</s:layout-render>