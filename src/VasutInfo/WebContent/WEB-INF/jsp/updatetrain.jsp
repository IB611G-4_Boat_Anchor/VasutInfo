<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="Update a Train">
	<s:layout-component name="body">
		<s:errors/>
		<s:messages/>
		<s:form beanclass="action.UpdateTrainActionBean">
			<table>
		        <tr>
		          <td>Train name:</td>
		          <td><s:select name="selectedID">
							<s:option value="">Select a train</s:option>
							<s:options-collection collection="${actionBean.list}" value="ID" label="name"/>
						</s:select>
				  </td>
		        </tr>
		        <tr>
		          <td>New name:</td>
		          <td><s:text name="newName"/></td>
		        </tr>
		        
      		</table>
      		<s:submit name="updateTrain" value="Update Train"/>
		</s:form>
	</s:layout-component>
</s:layout-render>