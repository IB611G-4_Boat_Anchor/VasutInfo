<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
  title="Add Discount">
  <s:layout-component name="body">
  	<s:errors/>
    <p>Add a new discount</p>
    <s:form beanclass="action.AddDiscountActionBean">
		<table>
		        <tr>
		          <td>Discount name:</td>
		          <td><s:text name="discountName"/></td>
		        </tr>
		        <tr>
		          <td>Discount percentage:</td>
		          <td><s:text name="percentage"/></td>
		        </tr>
      		</table>
      <s:submit name="addDiscount" value="Add Discount"/>
    </s:form>
  </s:layout-component>
</s:layout-render>