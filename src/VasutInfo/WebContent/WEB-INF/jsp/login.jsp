<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_general.jsp"
  title="Login">
  <s:layout-component name="body">
  	<s:errors/>
    <s:form beanclass="action.LoginActionBean">
      <table>
        <tr>
          <td>User name:</td>
          <td><s:text name="username"/></td>
        </tr>
        <tr>
          <td>Password:</td>
          <td><s:password name="password"/></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <s:submit name="login" value="Login"/>
            <s:submit name="register" value="Register"/>
          </td>
        </tr>
      </table>
    </s:form>
  </s:layout-component>
</s:layout-render>