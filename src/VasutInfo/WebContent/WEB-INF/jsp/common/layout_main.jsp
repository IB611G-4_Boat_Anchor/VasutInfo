<%@page contentType="text/html;charset=ISO-8859-1" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-definition>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd" >
	<html>
		<head>
			<title>${title}</title>
			<link rel="stylesheet" type="text/css"
			href="${contextPath}/css/style.css" >
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
			<script type="text/javascript" src="${contextPath}/js/flexdropdown.js"></script>
		</head>
		<body>
			<div id="header"><h1>Vas�t Info</h1>
			<div id="menu">
				<ul id="v_menu">
					<li><a href="#" data-flexmenu="fm_w">Wagons</a>
						<ul  id="fm_w" class="flexdropdownmenu">
							<li><a href="AddWagon.action">Add</a></li>
<!-- 							<li><a href="#">Remove</a></li> -->
							<li><a href="ListWagon.action">List</a></li>
<!-- 							<li><a href="#">Update</a></li> -->
						</ul>
					</li>
					<li><a href="#" data-flexmenu="fm_t">Trains</a>
						<ul  id="fm_t" class="flexdropdownmenu">
							<li><a href="AddTrain.action">Add</a></li>
							<li><a href="DeleteTrain.action">Remove</a></li>
							<li><a href="ListTrain.action">List</a></li>
							<li><a href="UpdateTrain.action">Update</a></li>
						</ul>
					</li>
					<li><a href="#" data-flexmenu="fm_l">Lines</a>
						<ul  id="fm_l" class="flexdropdownmenu">
							<li><a href="#">Add</a></li>
							<li><a href="#">Remove</a></li>
							<li><a href="#">Update</a></li>
							<li><a href="SearchLine.action">Search</a></li>
						</ul>
					</li>
					<li><a href="#" data-flexmenu="fm_s">Stations</a>
						<ul  id="fm_s" class="flexdropdownmenu">
							<li><a href="AddStation.action">Add</a></li>
							<li><a href="RemoveStation.action">Remove</a></li>
							<li><a href="ListStation.action">List</a></li>
							<li><a href="UpdateStation.action">Update</a></li>
						</ul>
					</li>
					<li><a href="#" data-flexmenu="fm_d">Discounts</a>
						<ul  id="fm_d" class="flexdropdownmenu">
							<li><a href="AddDiscount.action">Add</a></li>
							<li><a href="DeleteDiscount.action">Delete</a></li>
							<li><a href="ListDiscount.action">List</a></li>
							<li><a href="UpdateDiscount.action">Update</a></li>
						</ul>
					</li>
				</ul>
			</div>
			</div>
			<div id="body">
			<h2 id="title">${title}</h2>
				<s:layout-component name="body"/>
			</div>
			
			<div id="footer"><p>Copyright � - RF Team</p></div>
		</body>
	</html>
</s:layout-definition>

