<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
  title="Delete Train">
  <s:layout-component name="body">
  	<s:errors/>
  	<s:messages/>
    <p>Select a train to delete.</p>
    <s:form beanclass="action.DeleteTrainActionBean">
		<div><s:select name="selectedTrain">
			<s:option value="">Select a train...</s:option>
			<s:options-collection collection="${actionBean.trains}" value="ID" label="name"/>
		</s:select></div>
      <s:submit name="deleteTrain" value="Delete Train" />
    </s:form>
  </s:layout-component>
</s:layout-render>