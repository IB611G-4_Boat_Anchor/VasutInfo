<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="Add Wagon">
	<s:layout-component name="body">
		<s:messages/>
		<s:form beanclass="action.AddWagonActionBean">
			<table>
        		<tr>
         			<td>Train name:</td>
          			<td><s:select name="trainID">
						<s:option value="">Select a train...</s:option>
						<s:options-collection collection="${actionBean.trains}" value="ID" label="name"/>
						</s:select>
					</td>
        		</tr>
        		<tr>
         			<td>Bicycle:</td>
          			<td><c:forEach var="bicycle" items="${actionBean.enums}">
              				<s:radio name="bicycle" value="${bicycle}"/>${bicycle}
            			</c:forEach>
            		</td>
        		</tr>
        		<tr>
         			<td>Smoking:</td>
          			<td><c:forEach var="smoking" items="${actionBean.enums}">
              				<s:radio name="smoking" value="${smoking}"/>${smoking}
            			</c:forEach>
            		</td>
        		</tr>
        		<tr>
         			<td>Buffet Car:</td>
          			<td><c:forEach var="buffetCar" items="${actionBean.enums}">
              				<s:radio name="buffetCar" value="${buffetCar}"/>${buffetCar}
            			</c:forEach>
            		</td>
        		</tr>
        		<tr>
         			<td>Class number:</td>
          			<td><c:forEach var="classNumber" items="${actionBean.classes}">
              				<s:radio name="classNumber" value="${classNumber}"/>${classNumber}
            			</c:forEach>
            		</td>
        		</tr>
      		</table>
      		
      		<s:submit name="addWagon" value="Add Wagon"/>
		</s:form>
	</s:layout-component>
</s:layout-render>