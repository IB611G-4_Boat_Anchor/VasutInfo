<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="Wagon List">
	<s:layout-component name="body">
		<s:form beanclass="action.ListWagonActionBean">
			<d:table name="${actionBean.list}" id="wagon" requestURI="" defaultsort="1"
					 defaultorder="descending">
				<d:column title="Wagon id" property="ID" sortable="true" />
				<d:column title="Bicycle" property="bicycle"/>
				<d:column title="Smoking" property="smoking"/>
				<d:column title="Buffet Car" property="buffetCar"/>
				<d:column title="Class number" property="classNumber"/>
				<d:column title="TrainID" property="trainID" sortable="true"/>
			</d:table>
		</s:form>
	</s:layout-component>
</s:layout-render>