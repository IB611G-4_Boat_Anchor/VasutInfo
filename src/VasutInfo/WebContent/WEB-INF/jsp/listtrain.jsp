<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="Train List">
	<s:layout-component name="body">
		<s:form beanclass="action.ListTrainActionBean">
			<d:table name="${actionBean.list}" id="benchmark" requestURI="" defaultsort="1"
					 defaultorder="descending">
				<d:column title="Train id" property="ID" sortable="true" />
				<d:column title="Train name" property="name" sortable="true" />
			</d:table>
		</s:form>
	</s:layout-component>
</s:layout-render>