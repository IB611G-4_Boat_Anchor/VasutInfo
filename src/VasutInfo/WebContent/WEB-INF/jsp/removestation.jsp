<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="Remove Station">
	<s:layout-component name="body">
		<s:errors/>
		<s:messages/>
		<s:form beanclass="action.RemoveStationActionBean">
			<table>
		        <tr>
		          <td>Select a station:</td>
		          <td><s:select name="selectedStation">
							<s:option value="">Select a station</s:option>
							<s:options-collection collection="${actionBean.list}" value="ID" label="name"/>
						</s:select></td>
		        </tr>
      		</table>
      		<s:submit name="removeStation" value="Delete Station"/>
		</s:form>
	</s:layout-component>
</s:layout-render>