<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
  title="Delete Discount">
  <s:layout-component name="body">
  	<s:errors/>
    <p>Select a discount to delete.</p>
    <s:form beanclass="action.DeleteDiscountActionBean">
		<div><s:select name="selectedDiscount">
			<s:option value="">Select a discount...</s:option>
			<s:options-collection collection="${actionBean.discounts}" value="ID" label="name"/>
		</s:select></div>
      <s:submit name="deleteDiscount" value="Delete Discount"/>
    </s:form>
  </s:layout-component>
</s:layout-render>