<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="Update a Station">
	<s:layout-component name="body">
		<s:form beanclass="action.UpdateStationActionBean">
			<table>
		        <tr>
		          <td>Station name:</td>
		          <s:select name="selectedID">
							<s:option value="">Select a station</s:option>
							<s:options-collection collection="${actionBean.list}" value="ID" label="name"/>
						</s:select>
		        </tr>
		        <tr>
		          <td>New name:</td>
		          <td><s:text name="newName"/></td>
		        </tr>
		        
      		</table>
      		<s:submit name="updateStation" value="Update Station"/>
		</s:form>
	</s:layout-component>
</s:layout-render>