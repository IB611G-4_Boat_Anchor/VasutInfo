<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
  title="Add Train">
  <s:layout-component name="body">
  	<s:messages/>
  	<s:errors/>
    <p>Push the button below to add a new train.</p>
    <s:form beanclass="action.AddTrainActionBean">
      <table>
        <tr>
          <td>Train name:</td>
          <td><s:text name="trainName"/></td>
        </tr>
      </table>
      <s:submit name="addTrain" value="Add Train"/>
    </s:form>
  </s:layout-component>
</s:layout-render>