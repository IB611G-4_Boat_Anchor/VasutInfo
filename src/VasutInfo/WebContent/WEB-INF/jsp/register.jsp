<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_general.jsp"
  title="Registration">
  <s:layout-component name="body">
  	<s:errors/>
    <p>Register to create an account:</p>
    <s:form beanclass="action.RegisterActionBean">
      <table>
        <tr>
          <td>Name:</td>
          <td><s:text name="name"/></td>
        </tr>
        <tr>
          <td>User name:</td>
          <td><s:text name="username"/></td>
        </tr>
        <tr>
          <td>Password:</td>
          <td><s:password name="password"/></td>
        </tr>
        <tr>
          <td>Confirm Password:</td>
          <td><s:password name="confirmPassword"/></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <s:submit name="register" value="Register"/>
            <s:submit name="cancel" value="Cancel"/>
          </td>
        </tr>
      </table>
    </s:form>
  </s:layout-component>
</s:layout-render>