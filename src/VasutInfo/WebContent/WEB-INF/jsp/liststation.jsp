<%@page contentType="text/html;charset=ISO-8859-1" language="java"%>

<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_main.jsp"
	title="List of Stations">
	<s:layout-component name="body">
		<s:form beanclass="action.ListStationActionBean">
			<d:table name="${actionBean.list}" id="station" requestURI="" defaultsort="1"
					 defaultorder="ascending">
				<d:column title="Station id" property="ID" sortable="true" />
				<d:column title="Station name" property="name" sortable="true" />
			</d:table>
		</s:form>
	</s:layout-component>
</s:layout-render>