package model;

public class Wagon {
	public Wagon(int ID, boolean bicycle, boolean smoking, boolean buffetCar, int classNumber, int trainID){
		this.ID = ID;
		this.buffetCar = buffetCar;
		this.classNumber = classNumber;
		this.smoking = smoking;
		this.bicycle = bicycle;
		this.trainID = trainID;
		
	}
	
	private Integer ID;
	private boolean buffetCar;
	private Integer classNumber;
	private boolean smoking;
	private boolean bicycle;
	private Integer trainID;
	
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public boolean isBuffetCar() {
		return buffetCar;
	}
	public void setBuffetCar(boolean buffetCar) {
		this.buffetCar = buffetCar;
	}
	public int getClassNumber() {
		return classNumber;
	}
	public void setClassNumber(int classNumber) {
		this.classNumber = classNumber;
	}
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isBicycle() {
		return bicycle;
	}
	public void setBicycle(boolean bicycle) {
		this.bicycle = bicycle;
	}
	
	public Integer getTrainID() {
		return trainID;
	}
	public void setTrainID(Integer trainID) {
		this.trainID = trainID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	@Override
	public boolean equals(Object obj) {
		try { return ID.equals(((Wagon) obj).getID()); }
		catch (Exception exc) { return false; }
	}
	@Override
	public int hashCode() {
	return 31 + ((ID == null) ? 0 : ID.hashCode());
	}
}
