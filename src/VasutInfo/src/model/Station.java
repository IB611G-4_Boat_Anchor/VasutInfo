package model;

public class Station {
	Integer ID;
	String name;
	public Station(Integer iD, String name) {
		super();
		ID = iD;
		this.name = name;
	}
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
