package model;

public class Discount {
	public Discount(int ID, String name, int percentage){
		this.ID = ID;
		this.name = name;
		this.percentage = percentage;
	}
	
	private Integer ID;
	private String name;
	private Integer percentage;
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
	
	
}
