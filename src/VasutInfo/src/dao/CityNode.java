package dao;

public class CityNode {
	int cityID;
	int distance;
	int d;
	int parent;
	
	public CityNode(int cityID, int distance, int d, int parent) {
		this.cityID = cityID;
		this.distance = distance;
		this.d = d;
		this.parent = parent;
	}
	
	public int getCityID() {
		return cityID;
	}
	public void setCityID(int cityID) {
		this.cityID = cityID;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getD() {
		return d;
	}
	public void setD(int d) {
		this.d = d;
	}
	public int getParent() {
		return parent;
	}
	public void setParent(int parent) {
		this.parent = parent;
	}
	
	
}
