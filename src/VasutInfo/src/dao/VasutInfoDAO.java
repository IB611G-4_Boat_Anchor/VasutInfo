package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import model.Discount;
import model.Station;
import model.Train;
import model.Wagon;

public class VasutInfoDAO {
	
	//TODO: ezeket ki kell tolni egy property file-ba
	private static final String url = "jdbc:mysql://localhost/vasutinfo";
	private static final String db_user = "root";
	private static final String db_pass = ""; 
	private static final String driver = "com.mysql.jdbc.Driver";
	
	private static Connection kapcsolat;
	
	private static final String SQL_addWagon = 
			"insert into kocsi (Bicikli, Dohanyzo, Etkezo, Osztaly, VonatId) values " +
			"(?,?,?,?,?,?)";
	private static final String SQL_updateWagon = "update kocsi set Osztaly=? where KocsiID=?";
	private static final String SQL_addTrain = "insert into vonat(VonatID) values " + "(?)";
	private static final String SQL_updateTrain = "update vonat set VonatID=? where VonatID=?";
	
	
	
//	public static VasutInfoDAO getInstance(){
//		if(instance == null){
//			instance = new VasutInfoDAO();
//		}
//		return instance;
//	}
	
	public VasutInfoDAO() {
		
        try
        {
            Class.forName (driver);
            kapcsolat = DriverManager.getConnection (url, db_user, db_pass);
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        }
	}
	
	public Integer getUserID(String username){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("select felhasznaloID from felhasznalo where felhasznalonev = ?");
			pstmt.setString(1, username);
			rs = pstmt.executeQuery();
			if(rs.next()){
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean isAdmin(int userID){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("select adminID from admin where adminid = ?");
			pstmt.setInt(1, userID);
			rs = pstmt.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public String getPass(String username){
		try {
			PreparedStatement pstmt = kapcsolat.prepareStatement("select jelszo from felhasznalo where felhasznalonev = ?");
			pstmt.setString(1, username);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				return rs.getString(1);
			}
			
			
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean hasUserAlreadyExists(String username){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("select * from felhasznalo where felhasznalonev = ?");
			pstmt.setString(1, username);
			rs = pstmt.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void register(String name, String username, String password){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("insert into felhasznalo (nev, felhasznalonev, jelszo) " +
					"values (?,?,?)");
			pstmt.setString(1, name);
			pstmt.setString(2, username);
			pstmt.setString(3, password);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void buyTicket(int JegyID, Date Datum, int JaratId,int FelhasznaloID,int KedvezmegyID){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("insert into jegyetvesz (JegyId, Datum, JaratID, FelhasznaloID, KedvezmegyID) " +
					"values (?,?,?,?,?)");
			pstmt.setInt(1, JegyID);
			pstmt.setString(2, Datum);
			pstmt.setInt(3, JaratID);
			pstmt.setInt(4, FelhasznalID);
			pstmt.setInt(5, KedvezmegyID);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public void addTrain(String name) {
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("insert into vonat (vonatnev) values (?)");
			pstmt.setString(1, name);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void deleteTrain(int id){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("delete from vonat where vonatid = ?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public List<Train> getTrains(){
		Statement stmt;
		ResultSet rs;
		List<Train> list = new ArrayList<Train>();
		try {
			stmt = kapcsolat.createStatement();
			rs = stmt.executeQuery("select * from vonat");
			while(rs.next()){
				list.add(new Train(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
		
	}
	
	public Train getTrain(int id){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT vonatnev FROM vonat WHERE vonatid = ?");
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				return (new Train(id, rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateTrain(Train train, String newName) {
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("update vonat set vonatnev=? where vonatid = ?");
			pstmt.setString(1, newName);
			System.out.println(train.getID());
			pstmt.setInt(2, train.getID());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addWagon(boolean bicycle, boolean smoking, boolean buffetCar, int classNum, int trainID){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("insert into kocsi (bicikli, dohanyzo, etkezo, osztaly, vonatid) values (?,?,?,?,?)");
			pstmt.setBoolean(1, bicycle);
			pstmt.setBoolean(2, smoking);
			pstmt.setBoolean(3, buffetCar);
			pstmt.setInt(4, classNum);
			pstmt.setInt(5, trainID);
			pstmt.executeUpdate();
			//TODO: Hiba a tarolaskor a classnumnal
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public List<Wagon> getWagons(){
		Statement stmt;
		ResultSet rs;
		List<Wagon> list = new ArrayList<Wagon>();
		try {
			stmt = kapcsolat.createStatement();
			rs = stmt.executeQuery("select * from kocsi");
			while(rs.next()){
				list.add(new Wagon(rs.getInt(1), rs.getBoolean(2), rs.getBoolean(3),rs.getBoolean(4),rs.getInt(5)+ 1,rs.getInt(6)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<Discount> getDiscounts(){
		Statement stmt;
		ResultSet rs;
		List<Discount> list = new ArrayList<Discount>();
		try {
			stmt = kapcsolat.createStatement();
			rs = stmt.executeQuery("select * from kedvezmeny");
			while(rs.next()){
				list.add(new Discount(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
		
	}
	
	public int distance(Integer city1ID, Integer city2ID){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("select tavolsag from tavolsag where (egyikid=? and masikid=?) or(egyikid=? and masikid=?)");
			pstmt.setInt(1, city1ID);
			pstmt.setInt(2, city2ID);
			pstmt.setInt(3, city2ID);
			pstmt.setInt(4, city1ID);
			rs = pstmt.executeQuery();
			if(rs.next()){
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int numberOfCities(){
		Statement stmt;
		ResultSet rs;
		try {
			stmt = kapcsolat.createStatement();
			rs = stmt.executeQuery("SELECT COUNT(allomasID) FROM allomas");
			if(rs.next()){
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getCityID(String cityName){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT allomasID FROM allomas where neve = ?");
			pstmt.setString(1, cityName);
			rs = pstmt.executeQuery();
			if(rs.next()){
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<Station> getStations(){
		Statement stmt;
		ResultSet rs;
		List<Station> result = new ArrayList<Station>();
		try {
			stmt = kapcsolat.createStatement();
			rs = stmt.executeQuery("SELECT * FROM allomas");
			while(rs.next()){
				result.add(new Station(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Station getStation(int id){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT allomasid,neve FROM allomas WHERE allomasid = ?");
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				return (new Station(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addStation(String name){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("insert into allomas (neve) values (?)");
			pstmt.setString(1, name);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeStation(int id){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("delete from allomas where allomasid = ?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateStation(Station s, String newName){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("update allomas set neve=? where allomasid = ?");
			pstmt.setString(1, newName);
			pstmt.setInt(2, s.getID());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
//	public List<CityNode> getShortestLine(){
//		//init graph
//		List<LinkedList<CityNode>> graph = new ArrayList<LinkedList<CityNode>>();
//		List<Station> stations = get
//		
//	}
	
	
	
	public boolean updateWagon(Wagon w) {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			
			conn = DriverManager.getConnection(url, db_user, db_pass);
			
			
			pst = conn.prepareStatement(SQL_updateWagon);
			
			int index = 1;
			
			pst.setInt(index++, w.getClassNumber());
			pst.setInt(index++, w.getID()); // nemjo
				
			pst.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			try {
				if(pst != null)
					pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean updateTrain(Train t) {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			
			conn = DriverManager.getConnection(url, db_user, db_pass);
			
			
			pst = conn.prepareStatement(SQL_updateTrain);
			
			int index = 1;
			
			
			pst.setInt(index++, t.getID()); // nemjo
				
			pst.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			try {
				if(pst != null)
					pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public void addDiscount(String neve, int percentage) {
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("insert into kedvezmeny (Neve, Foka) values (?,?)");
		    pstmt.setString(1, neve);
		    pstmt.setInt(2, percentage);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateDiscount(Discount d) {
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("update kedvezmeny set Neve=?, Foka=? where KedvezmenyID = ?");
			int index = 0;
		    
			pstmt.setInt(1, d.getID());
		    pstmt.setString(2, d.getName());
		    pstmt.setInt(3, d.getPercentage());
		    
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteDiscount(int id){
		PreparedStatement pstmt;
		try {
			pstmt = kapcsolat.prepareStatement("delete from kedvezmeny where KedvezmenyID = ?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public Discount getDiscount(int id){
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT neve FROM kedvezmeny WHERE kedvezmenyid = ?");
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				return (new Discount(id, rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean checkIfTrainExists(Train t) {
		PreparedStatement pstmt;
		ResultSet rs;
		boolean ex = false;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT * FROM vonat WHERE vonatid = ?");
			pstmt.setInt(1, t.getID());
			rs = pstmt.executeQuery();
			if (rs.next()) ex = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ex;
	}
	
	private boolean checkIfDiscountExists(Discount d) {
		PreparedStatement pstmt;
		ResultSet rs;
		boolean ex = false;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT * FROM kedvezmeny WHERE kedvezmenyid = ?");
			pstmt.setInt(1, d.getID());
			pstmt.setString(2, d.getName());
			pstmt.setInt(3, d.getPercentage());
			rs = pstmt.executeQuery();
			if (rs.next()) ex = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ex;
	}
	
	private boolean checkIfWagonExists(Wagon w) {
		PreparedStatement pstmt;
		ResultSet rs;
		boolean ex = false;
		try {

			pstmt = kapcsolat.prepareStatement("SELECT * FROM kocsi WHERE kocsiID = ?");
			pstmt.setInt(1, w.getID());
			pstmt.setBoolean(2, w.isBicycle());
			pstmt.setBoolean(3, w.isSmoking());
			pstmt.setBoolean(4, w.isBuffetCar());
			pstmt.setInt(5, w.getClassNumber());
			pstmt.setInt(6, w.getTrainID());			
			rs = pstmt.executeQuery();
			if (rs.next()) ex = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ex;
	}
	
	private boolean checkIfStationExists(Station s) {
		PreparedStatement pstmt;
		ResultSet rs;
		boolean ex = false;
		try {
			pstmt = kapcsolat.prepareStatement("SELECT * FROM allomas WHERE allomasid = ?");
			pstmt.setInt(1, s.getID());
			pstmt.setString(2, s.getName());
			rs = pstmt.executeQuery();
			if (rs.next()) ex = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ex;
	}
}