package action;

import dao.VasutInfoDAO;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.ValidationMethod;

public class RegisterActionBean extends BaseActionBean {
	private String name;
	private String username;
	private String password;
	private String confirmPassword;
	
	private static final String VIEW = "/WEB-INF/jsp/register.jsp";
	private static final String LOGIN = "/WEB-INF/jsp/login.jsp";
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution register(){
		VasutInfoDAO dao = new VasutInfoDAO();
		dao.register(name, username, password);
		return new ForwardResolution(LOGIN);
	}
    
	public Resolution cancel(){
		return new ForwardResolution(LOGIN);
	}
	
	@ValidationMethod(on="register")
	public void validatePassword(){
	if (!password.equals(confirmPassword)) {
		getContext().getValidationErrors().addGlobalError(
				new SimpleError("The passwords do not match." ));
		}
	}
	
	@ValidationMethod(on="register")
	public void usernameAlreadyExists(){
		VasutInfoDAO dao = new VasutInfoDAO();
		if (dao.hasUserAlreadyExists(username)) {
			getContext().getValidationErrors().addGlobalError(
					new SimpleError("This user name has already taken." ));
		}
	}
	

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}

