package action;

import java.util.List;

import dao.VasutInfoDAO;

import model.Station;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;

public class AddStationActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/addstation.jsp";
	
	private String name;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution addStation(){
		VasutInfoDAO dao = new VasutInfoDAO();
		getContext().getMessages().add(new SimpleMessage("{0} station added." , name));
		dao.addStation(name);
		return new RedirectResolution(getClass());
	}
	
	public List<Station> getList(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getStations();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
