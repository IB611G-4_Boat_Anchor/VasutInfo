package action;
import java.util.List;
import dao.VasutInfoDAO;
import model.Discount;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class ListDiscountActionBean extends BaseActionBean {

	private static final String VIEW = "/WEB-INF/jsp/listdiscount.jsp";
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public List<Discount> getDiscounts(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getDiscounts();
	}

}
