package action;

import java.util.List;

import dao.VasutInfoDAO;

import model.Station;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class UpdateStationActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/updatestation.jsp";
	
	private int selectedID;
	private String newName;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution updateStation(){
		VasutInfoDAO dao = new VasutInfoDAO();
		dao.updateStation(dao.getStation(selectedID), newName);
		return new ForwardResolution(VIEW);
	}
	
	public List<Station> getList(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getStations();
	}

	public int getId() {
		return selectedID;
	}

	public void setId(int selectedID) {
		this.selectedID = selectedID;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}
}
