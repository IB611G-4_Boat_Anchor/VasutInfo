package action;

import java.util.List;

import dao.VasutInfoDAO;
import model.Wagon;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class ListWagonActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/listwagon.jsp";
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public List<Wagon> getlist(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getWagons();
	}
}
