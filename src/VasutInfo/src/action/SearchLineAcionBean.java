package action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class SearchLineAcionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/searchline.jsp";
	
	private String start;
	private String stop;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution search(){
		
		return new ForwardResolution(VIEW);
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getStop() {
		return stop;
	}

	public void setStop(String stop) {
		this.stop = stop;
	}
	
	
	
}
