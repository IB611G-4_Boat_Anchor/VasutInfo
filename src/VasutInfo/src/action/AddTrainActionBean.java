package action;

import dao.VasutInfoDAO;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;
import net.sourceforge.stripes.validation.Validate;

public class AddTrainActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/addtrain.jsp";
	
	@Validate(required=true, on="addTrain")
	private String trainName;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution addTrain(){
		VasutInfoDAO dao = new VasutInfoDAO();
		dao.addTrain(trainName);
		getContext().getMessages().add(new SimpleMessage("A new train: {0} added." , trainName));
		return new RedirectResolution(getClass());
	}

	public String getTrainName() {
		return trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}
	
	
}
