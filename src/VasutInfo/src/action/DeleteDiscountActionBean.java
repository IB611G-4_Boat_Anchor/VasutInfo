package action;
import java.util.List;
import dao.VasutInfoDAO;
import model.Discount;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class DeleteDiscountActionBean extends BaseActionBean {

	private static final String VIEW = "/WEB-INF/jsp/deletediscount.jsp";
	
	private Integer selectedDiscount;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution deleteDiscount(){
		VasutInfoDAO dao = new VasutInfoDAO();
		dao.deleteDiscount(selectedDiscount);
		return new ForwardResolution(VIEW);
	}
	
	public List<Discount> getDiscounts(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getDiscounts();
	}

	public Integer getSelectedDiscount() {
		return selectedDiscount;
	}

	public void setSelectedDiscount(Integer selectedDiscount) {
		this.selectedDiscount = selectedDiscount;
	}
}
