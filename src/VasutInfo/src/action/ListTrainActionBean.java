package action;

import java.util.List;

import dao.VasutInfoDAO;
import model.Train;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class ListTrainActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/listtrain.jsp";
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	
	public List<Train> getList(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getTrains();
	}
}
