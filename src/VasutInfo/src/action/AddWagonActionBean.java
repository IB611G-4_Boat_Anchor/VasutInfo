package action;

import java.util.List;

import dao.VasutInfoDAO;

import model.Train;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;

public class AddWagonActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/addwagon.jsp";
	
	private boolean bicycle;
	private boolean smoking;
	private boolean buffetCar;
	private int classNumber;
	private int trainID;

	public String[] getEnums() {
        String[] s = {"yes","no"};
        return s;
    }
	
	public int[] getClasses() {
        int[] i = {1,2};
        return i;
    }

	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution addWagon(){
		VasutInfoDAO dao = new VasutInfoDAO();
		dao.addWagon(bicycle, smoking, buffetCar, classNumber, trainID);
		getContext().getMessages().add(new SimpleMessage("Added a wagon to {0} train." , dao.getTrain(trainID).getName()));
		return new RedirectResolution(getClass());
	}
	
	public List<Train> getTrains(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getTrains();
	}
	
	
	public boolean isBicycle() {
		return bicycle;
	}


	public void setBicycle(boolean bicycle) {
		this.bicycle = bicycle;
	}


	public boolean isSmoking() {
		return smoking;
	}


	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}


	public boolean isBuffetCar() {
		return buffetCar;
	}


	public void setBuffetCar(boolean buffetCar) {
		this.buffetCar = buffetCar;
	}


	public int getClassNumber() {
		return classNumber;
	}


	public void setClassNumber(int classNumber) {
		this.classNumber = classNumber;
	}


	public int getTrainID() {
		return trainID;
	}


	public void setTrainID(int trainID) {
		this.trainID = trainID;
	}
}
