package action;

import java.util.List;

import dao.VasutInfoDAO;

import model.Train;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;

public class UpdateTrainActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/updatetrain.jsp";

	private int selectedID;
	private String newName;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution updateTrain(){
		VasutInfoDAO dao = new VasutInfoDAO();
		getContext().getMessages().add(
				new SimpleMessage("{0} updated to {1} .", dao.getTrain(selectedID).getName() , newName));
		dao.updateTrain(dao.getTrain(selectedID), newName);
		return new RedirectResolution(getClass());
	}
	
	public List<Train> getList(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getTrains();
	}
	
	public int getSelectedID() {
		return selectedID;
	}

	public void setSelectedID(int selectedID) {
		this.selectedID = selectedID;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}
}
