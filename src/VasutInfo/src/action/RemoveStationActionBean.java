package action;

import java.util.List;

import dao.VasutInfoDAO;

import model.Station;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;

public class RemoveStationActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/removestation.jsp";
	
	private Integer selectedStation;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution removeStation(){
		VasutInfoDAO dao = new VasutInfoDAO();
		System.out.println(selectedStation);
		getContext().getMessages().add(new SimpleMessage("Deleted {0} station." , dao.getStation(selectedStation).getName()));
		dao.removeStation(selectedStation);
		return new RedirectResolution(getClass());
	}
	
	public List<Station> getList(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getStations(); 
	}

	public Integer getId() {
		return selectedStation;
	}

	public void setId(Integer selectedStation) {
		this.selectedStation = selectedStation;
	}
	
}
