package action;
import java.util.List;
import dao.VasutInfoDAO;
import model.Discount;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class AddDiscountActionBean extends BaseActionBean {

	private static final String VIEW = "/WEB-INF/jsp/adddiscount.jsp";
	
	private String discountName;
	private Integer percentage;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution addDiscount(){
		VasutInfoDAO dao = new VasutInfoDAO();
		dao.addDiscount(discountName, percentage);
		return new ForwardResolution(VIEW);
	}

	public String getDiscountName() {
		return discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
	
	
	
}
