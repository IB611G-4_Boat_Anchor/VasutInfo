package action;

import dao.VasutInfoDAO;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.Validate;;


public class LoginActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/login.jsp";
	private static final String ADMINLOGIN = "/WEB-INF/jsp/addtrain.jsp";
	private static final String USERLOGIN = "/WEB-INF/jsp/startuser.jsp";
	private static final String REGISTER = "/WEB-INF/jsp/register.jsp";
	
	@Validate(required=true, on="login")
	private String username;
	@Validate(required=true, on="login")
	private String password;
	
	@DefaultHandler
	public Resolution view(){
		return new ForwardResolution(VIEW);
	}
	
	public Resolution login(){
		//TODO: meg kell n�zni hogy adatb�zisbeli cuccokkal megegyezik-e a dolog.
		VasutInfoDAO dao = new VasutInfoDAO();
		
		if(password.equals(dao.getPass(username))){
			if(dao.isAdmin(dao.getUserID(username))){
				return new ForwardResolution(ADMINLOGIN);
			}
			else{
				return new ForwardResolution(USERLOGIN);
			}
		}
		return new ForwardResolution(VIEW);
		
		//TODO: k�l�nben hib�s jelsz�
		
	}
	
	public Resolution register(){
		return new ForwardResolution(REGISTER);
	}
	
	
	//getters and setters
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
