package action;

import java.util.List;

import dao.VasutInfoDAO;

import model.Station;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

public class ListStationActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/liststation.jsp";
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public List<Station> getList(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getStations();
	}

}
