package action;

import java.util.List;

import dao.VasutInfoDAO;
import model.Train;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;

public class DeleteTrainActionBean extends BaseActionBean{
	private static final String VIEW = "/WEB-INF/jsp/deletetrain.jsp";
	
	private Integer selectedTrain;
	
	@DefaultHandler
    public Resolution view() {
        return new ForwardResolution(VIEW);
    }
	
	public Resolution deleteTrain(){
		VasutInfoDAO dao = new VasutInfoDAO();
		getContext().getMessages().add(new SimpleMessage("Deleted {0}." , dao.getTrain(selectedTrain).getName()));
		dao.deleteTrain(selectedTrain);
		return new RedirectResolution(getClass());
	}
	
	public List<Train> getTrains(){
		VasutInfoDAO dao = new VasutInfoDAO();
		return dao.getTrains();
	}

	public Integer getSelectedTrain() {
		return selectedTrain;
	}

	public void setSelectedTrain(Integer selectedTrain) {
		this.selectedTrain = selectedTrain;
	}
}



