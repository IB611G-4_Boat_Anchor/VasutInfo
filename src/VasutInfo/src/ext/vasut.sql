

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- Adatbázis: `vasut`

-- --------------------------------------------------------

-- Tábla szerkezet: `admin`

CREATE TABLE IF NOT EXISTS `admin` (
  `AdminID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`AdminID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `felhasználó`
--

CREATE TABLE IF NOT EXISTS `felhasznalo` (
  `FelhasznaloID` int(11) NOT NULL AUTO_INCREMENT,
  `Nev` varchar(40) NOT NULL,
  `FelhasznaloNev` varchar(40) NOT NULL,
  `Jelszo` varchar(40) NOT NULL,
  PRIMARY KEY (`FelhasznaloID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- -------------------------------------------------------
-- Tábla szerkezet: `jegyetvesz`

CREATE TABLE IF NOT EXISTS `jegyetvesz` (
  `JegyID` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date NOT NULL,
  `JaratID` int(11) NOT NULL,
  `FelhasznaloID` int(11) NOT NULL,
  `KedvezmenyID` int(11) NOT NULL,
  PRIMARY KEY (`JegyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
-- Tábla szerkezet: `járat`

CREATE TABLE IF NOT EXISTS `jarat` (
  `JaratID` int(11) NOT NULL AUTO_INCREMENT,
  `VonatID` int(11) NOT NULL,
  `Indulasi-ido` date NOT NULL,
  `Erkezesi-ido` date NOT NULL,
  `HovaID` int(11) NOT NULL,
  `HonnanID` int(11) NOT NULL,
  PRIMARY KEY (`JaratID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `kedvezmény`
--

CREATE TABLE IF NOT EXISTS `kedvezmeny` (
  `KedvezmenyID` int(11) NOT NULL AUTO_INCREMENT,
  `Neve` varchar(40) NOT NULL,
  `Foka` int(11) NOT NULL,
  PRIMARY KEY (`KedvezmenyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `kocsi`
--

CREATE TABLE IF NOT EXISTS `kocsi` (
  `KocsiID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Bicikli` tinyint(1) NOT NULL,
  `Dohanyzo` tinyint(1) NOT NULL,
  `Etkezo` tinyint(1) NOT NULL,
  `Osztaly` int(11) NOT NULL,
  `VonatId` int(11) NOT NULL,
  PRIMARY KEY (`KocsiID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `távolság`
--

CREATE TABLE IF NOT EXISTS `tavolsag` (
  `Tavolsag` int(11) NOT NULL,
  `EgyikID` int(11) NOT NULL,
  `MasikID` int(11) NOT NULL,
  PRIMARY KEY (`EgyikID`,`MasikID`),
  KEY `MasikID` (`MasikID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `vonat`
--

CREATE TABLE IF NOT EXISTS `vonat` (
  `VonatID` int(11) NOT NULL AUTO_INCREMENT,
  `vonatnev` varchar(40) NOT NULL,
  PRIMARY KEY (`VonatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `állomás`
--

CREATE TABLE IF NOT EXISTS `allomas` (
  `AllomasID` int(11) NOT NULL AUTO_INCREMENT,
  `Neve` varchar(40) NOT NULL,
  PRIMARY KEY (`AllomasID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `távolság`
--
ALTER TABLE `tavolsag`
  ADD CONSTRAINT `tavolsag_ibfk_1` FOREIGN KEY (`EgyikID`) REFERENCES `allomas` (`AllomasID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tavolsag_ibfk_2` FOREIGN KEY (`MasikID`) REFERENCES `allomas` (`AllomasID`) ON DELETE CASCADE ON UPDATE CASCADE;

  
  ALTER TABLE `jegyetvesz`
  ADD CONSTRAINT `jegyetvesz_ibfk_1` FOREIGN KEY (`JaratID`) REFERENCES `jarat` (`JaratID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jegyetvesz_ibfk_2` FOREIGN KEY (`FelhasznaloID`) REFERENCES `felhasznalo` (`FelhasznaloID`) ON DELETE CASCADE ON UPDATE CASCADE,
   ADD CONSTRAINT `jegyetvesz_ibfk_3` FOREIGN KEY (`KedvezmenyID`) REFERENCES `kedvezmeny` (`KedvezmenyID`) ON DELETE CASCADE ON UPDATE CASCADE;
 
 ALTER TABLE `jarat`
  ADD CONSTRAINT `jarat_ibfk_1` FOREIGN KEY (`VonatID`) REFERENCES `vonat` (`VonatID`) ON DELETE CASCADE ON UPDATE CASCADE;
 
 ALTER TABLE `kocsi`
  ADD CONSTRAINT `kocsi_ibfk_1` FOREIGN KEY (`VonatID`) REFERENCES `vonat` (`VonatID`) ON DELETE CASCADE ON UPDATE CASCADE;
  
  

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
