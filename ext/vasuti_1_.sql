-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost
-- Létrehozás ideje: 2011. nov. 06. 10:18
-- Szerver verzió: 5.5.16
-- PHP verzió: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `vasuti(1)`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet: `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `AdminID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`AdminID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `admin`
--

INSERT INTO `admin` (`AdminID`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `allomas`
--

CREATE TABLE IF NOT EXISTS `allomas` (
  `AllomasID` int(11) NOT NULL AUTO_INCREMENT,
  `Neve` varchar(40) NOT NULL,
  PRIMARY KEY (`AllomasID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1016 ;

--
-- A tábla adatainak kiíratása `allomas`
--

INSERT INTO `allomas` (`AllomasID`, `Neve`) VALUES
(1001, 'Szeged'),
(1002, 'Hodmezovasarhely'),
(1003, 'Kiskunfelegyhaza'),
(1004, 'Cegled'),
(1005, 'Algyo'),
(1006, 'Bekescsaba'),
(1007, 'Debrecen'),
(1008, 'Gyor'),
(1009, 'Pecs'),
(1010, 'Mohacs'),
(1011, 'Budapest'),
(1012, 'Baja'),
(1013, 'Kecskemet'),
(1014, 'Szentes'),
(1015, 'Tatabanya');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `felhasznalo`
--

CREATE TABLE IF NOT EXISTS `felhasznalo` (
  `FelhasznaloID` int(11) NOT NULL AUTO_INCREMENT,
  `Nev` varchar(40) NOT NULL,
  `FelhasznaloNev` varchar(40) NOT NULL,
  `Jelszo` varchar(40) NOT NULL,
  PRIMARY KEY (`FelhasznaloID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- A tábla adatainak kiíratása `felhasznalo`
--

INSERT INTO `felhasznalo` (`FelhasznaloID`, `Nev`, `FelhasznaloNev`, `Jelszo`) VALUES
(1, 'Kiss Janos', 'KJani', 'kjani'),
(2, 'Kovacs Gabor', 'KGabor', 'kgabor'),
(3, 'Durva Aron', 'DAron', 'daron'),
(4, 'Tur Bela', 'TBela', 'tbela'),
(5, 'Alma Rozsa', 'ARozsa', 'arozsa'),
(6, 'Kovacs Anett', 'KAnett', 'kanett'),
(7, 'Katona Andras', 'KAndris', 'kbandi'),
(8, 'Toth Sandor', 'TSanyi', 'tsanya'),
(9, 'Makro Zsanett', 'MZsani', 'mzsani'),
(10, 'Nagy Janos', 'NJani', 'njani');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `jarat`
--

CREATE TABLE IF NOT EXISTS `jarat` (
  `JaratID` int(11) NOT NULL AUTO_INCREMENT,
  `VonatID` int(11) NOT NULL,
  `Indulasi-ido` date NOT NULL,
  `Erkezesi-ido` date NOT NULL,
  `HovaID` int(11) NOT NULL,
  `HonnanID` int(11) NOT NULL,
  PRIMARY KEY (`JaratID`),
  KEY `jarat_ibfk_1` (`VonatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- A tábla adatainak kiíratása `jarat`
--

INSERT INTO `jarat` (`JaratID`, `VonatID`, `Indulasi-ido`, `Erkezesi-ido`, `HovaID`, `HonnanID`) VALUES
(1, 3, '2011-11-13', '2011-11-13', 1001, 1002),
(2, 3, '2011-11-13', '2011-11-13', 1002, 1006),
(3, 4, '2011-11-14', '2011-11-14', 1001, 1003),
(4, 4, '2011-11-14', '2011-11-14', 1003, 1013),
(5, 4, '2011-11-14', '2011-11-14', 1013, 1004),
(6, 4, '2011-11-14', '2011-11-14', 1004, 1011);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `jegyetvesz`
--

CREATE TABLE IF NOT EXISTS `jegyetvesz` (
  `JegyID` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date NOT NULL,
  `JaratID` int(11) NOT NULL,
  `FelhasznaloID` int(11) NOT NULL,
  `KedvezmenyID` int(11) NOT NULL,
  PRIMARY KEY (`JegyID`),
  KEY `jegyetvesz_ibfk_1` (`JaratID`),
  KEY `jegyetvesz_ibfk_2` (`FelhasznaloID`),
  KEY `jegyetvesz_ibfk_3` (`KedvezmenyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `jegyetvesz`
--

INSERT INTO `jegyetvesz` (`JegyID`, `Datum`, `JaratID`, `FelhasznaloID`, `KedvezmenyID`) VALUES
(1, '2011-11-13', 1, 2, 2),
(2, '2011-11-14', 3, 5, 1),
(3, '2011-11-14', 4, 5, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `kedvezmeny`
--

CREATE TABLE IF NOT EXISTS `kedvezmeny` (
  `KedvezmenyID` int(11) NOT NULL AUTO_INCREMENT,
  `Neve` varchar(40) NOT NULL,
  `Foka` int(11) NOT NULL,
  PRIMARY KEY (`KedvezmenyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `kedvezmeny`
--

INSERT INTO `kedvezmeny` (`KedvezmenyID`, `Neve`, `Foka`) VALUES
(1, 'tanulo/nyugdijas', 2),
(2, 'rokkant', 1),
(3, 'nagy csalados', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `kocsi`
--

CREATE TABLE IF NOT EXISTS `kocsi` (
  `KocsiID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Bicikli` tinyint(1) NOT NULL,
  `Dohanyzo` tinyint(1) NOT NULL,
  `Etkezo` tinyint(1) NOT NULL,
  `Osztaly` int(11) NOT NULL,
  `VonatId` int(11) NOT NULL,
  PRIMARY KEY (`KocsiID`),
  KEY `kocsi_ibfk_1` (`VonatId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- A tábla adatainak kiíratása `kocsi`
--

INSERT INTO `kocsi` (`KocsiID`, `Bicikli`, `Dohanyzo`, `Etkezo`, `Osztaly`, `VonatId`) VALUES
(1, 1, 0, 1, 1, 1),
(2, 0, 0, 1, 2, 2),
(3, 1, 1, 1, 1, 3),
(4, 0, 0, 1, 2, 3),
(5, 1, 0, 0, 1, 4),
(6, 0, 1, 0, 2, 4),
(7, 0, 1, 1, 1, 5),
(8, 1, 0, 0, 2, 5),
(9, 1, 1, 0, 1, 4),
(10, 0, 0, 1, 2, 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `tavolsag`
--

CREATE TABLE IF NOT EXISTS `tavolsag` (
  `Tavolsag` int(11) NOT NULL,
  `EgyikID` int(11) NOT NULL,
  `MasikID` int(11) NOT NULL,
  PRIMARY KEY (`EgyikID`,`MasikID`),
  KEY `MasikID` (`MasikID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `tavolsag`
--

INSERT INTO `tavolsag` (`Tavolsag`, `EgyikID`, `MasikID`) VALUES
(20, 1001, 1002),
(100, 1001, 1003),
(80, 1002, 1006),
(30, 1003, 1013),
(30, 1004, 1011),
(40, 1004, 1013);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `vonat`
--

CREATE TABLE IF NOT EXISTS `vonat` (
  `VonatID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`VonatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- A tábla adatainak kiíratása `vonat`
--

INSERT INTO `vonat` (`VonatID`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10);

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `jarat`
--
ALTER TABLE `jarat`
  ADD CONSTRAINT `jarat_ibfk_1` FOREIGN KEY (`VonatID`) REFERENCES `vonat` (`VonatID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `jegyetvesz`
--
ALTER TABLE `jegyetvesz`
  ADD CONSTRAINT `jegyetvesz_ibfk_1` FOREIGN KEY (`JaratID`) REFERENCES `jarat` (`JaratID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jegyetvesz_ibfk_2` FOREIGN KEY (`FelhasznaloID`) REFERENCES `felhasznalo` (`FelhasznaloID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jegyetvesz_ibfk_3` FOREIGN KEY (`KedvezmenyID`) REFERENCES `kedvezmeny` (`KedvezmenyID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `kocsi`
--
ALTER TABLE `kocsi`
  ADD CONSTRAINT `kocsi_ibfk_1` FOREIGN KEY (`VonatId`) REFERENCES `vonat` (`VonatID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `tavolsag`
--
ALTER TABLE `tavolsag`
  ADD CONSTRAINT `tavolsag_ibfk_1` FOREIGN KEY (`EgyikID`) REFERENCES `allomas` (`AllomasID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tavolsag_ibfk_2` FOREIGN KEY (`MasikID`) REFERENCES `allomas` (`AllomasID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
