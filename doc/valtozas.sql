-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost
-- Létrehozás ideje: 2011. okt. 22. 09:24
-- Szerver verzió: 5.5.16
-- PHP verzió: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `jarat a jelenlegi`
--

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `jarat` (
  `JaratID` int(11) NOT NULL,
  `VonatID` int(11) NOT NULL,
  `Indulasi-ido` time NOT NULL,
  `Erkezesi-ido` time NOT NULL,
  `HovaID` int(11) NOT NULL,
  `HonnanID` int(11) NOT NULL,
  UNIQUE KEY `JaratID` (`JaratID`,`HonnanID`),
  KEY `jarat_ibfk_1` (`VonatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
--
-- Adatbázis: `jarat volt de a hiba ott volt,hogy nem lehet rendesen osszekapcsolni egy menetben 
-- külön jaratok voltak a szakaszok is`
--
CREATE TABLE IF NOT EXISTS `jarat` (
  `JaratID` int(11) NOT NULL AUTO_INCREMENT,
  `VonatID` int(11) NOT NULL,
  `Indulasi-ido` date NOT NULL,
  `Erkezesi-ido` date NOT NULL,
  `HovaID` int(11) NOT NULL,
  `HonnanID` int(11) NOT NULL,
  PRIMARY KEY (`JaratID`),
  KEY `jarat_ibfk_1` (`VonatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
